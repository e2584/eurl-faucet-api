const express = require( 'express' )
const app     = express()
const port    = process.env.PORT || 4000

const cors = require('cors')
app.use(cors({
    origin: ['http://localhost:3000', 'https://www.lugh.io']
}))

const taquito = require('@taquito/taquito')
const signer = require('@taquito/signer')
const faucet = require('./faucet/hangzhounet.json')

app.get( '/provision/:account',async(req, res) => {
    const account = req.params.account;
    if(!account || account.length !== 36)
        res.json({status: 'ERROR', message: 'Invalid account format'})
    else{
        const Tezos = new taquito.TezosToolkit('https://hangzhounet.smartpy.io/');
        Tezos.setSignerProvider(signer.InMemorySigner.fromFundraiser(faucet.email, faucet.password, faucet.mnemonic.join(' ')))
        try {
            const activation = await Tezos.tz.activate(faucet.pkh, faucet.activation_code)
            await activation.confirmation()
        } catch (e) {
            console.log('Already activated')
        }
        const contractEurl = await Tezos.contract.at("KT1StBf222xBLfLTChsute8F9HSt9kVHpqdY")
        const operation = await contractEurl.methods.transfer([
            {
              from_: faucet.pkh,
              txs: [
                {
                  to_: account,
                  token_id: 0,
                  amount: 100000000,
                },
              ],
            },
        ]).send()
        await operation.confirmation()
        res.json({status: 'SUCCESS', data:{ opHash: operation.hash, amount: 100, decimals: 6}})
    }
})
app.get( '/about', ( req, res ) => {
    res.type( 'text/plain' )
    res.send( 'An OPEN API to power EURL faucet')
})
app.use( ( req, res ) => {
    res.type( 'text/plain' )
    res.status( 404 )
    res.send('404 Not found ☕_☕')
})

app.listen( port ,
() => console.log(`EURL-API ☕ is on Port ${ port } Ctrl + C to Stop `) )